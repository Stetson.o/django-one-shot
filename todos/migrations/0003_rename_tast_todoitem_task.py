# Generated by Django 5.0 on 2023-12-12 21:21

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0002_todoitem"),
    ]

    operations = [
        migrations.RenameField(
            model_name="todoitem",
            old_name="tast",
            new_name="task",
        ),
    ]
