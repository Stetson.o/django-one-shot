# Generated by Django 5.0 on 2023-12-14 00:18

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0005_alter_todoitem_todolist"),
    ]

    operations = [
        migrations.RenameField(
            model_name="todoitem",
            old_name="todolist",
            new_name="list",
        ),
    ]
