from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.
def todo_list_list(request):
    list = TodoList.objects.all()
    context = {
        "todo_list_list": list,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todolist_object": todo_list,
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        list_form = TodoListForm(request.POST)
        if list_form.is_valid():
            list = list_form.save()
            return redirect("todo_list_detail", id=list.id)

    else:
        context = {
            "form": TodoListForm,
        }
    return render(request, "todos/create.html", context)

def todo_list_edit(request, id):
    post = TodoList.objects.get(id=id)
    if request.method == "POST":
        list_form = TodoListForm(request.POST, instance=post)
        if list_form.is_valid():
            list_form.save()
            return redirect("todo_list_detail", post.id)
    else:
        list_form = TodoListForm(instance=post)

    context = {
        "todolist_object": post,
        "form": list_form,
    }
    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect('todo_list_list')

    return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=form.instance.list.id)

    else:
        form = TodoItemForm()
    context = {
       "form": form,
    }
    return render(request, "todos/items/create.html", context)


def todo_item_edit(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=form.instance.list.id)
    else:
        form = TodoItemForm(instance=post)
    context = {
        "todoitem_object": post.list.id,
        "form": form
    }
    return render(request, "todos/items/edit.html", context)
